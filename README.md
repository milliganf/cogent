# Cogent
Cogent is a library for working with precise rational numbers. It provides 
simplification and basic operations with rational numbers, and has the 
ability to fall back to floats if necessary.

See the full documentation for more information.

## Examples
```rust
use cogent::Number;

fn main() {
    let a = &<Number>::from(720);
    let b = &<Number>::from(24);
    // Prints: 720/24 = 30
    println!("{a}/{b} = {}", a/b);

    let one_third = &<Number>::new_ratio(1, 3);
    // Prints: (1/3)*2  = (2/3)
    println!("{one_third}*2 = {}", one_third*Number::from(2));
}
```

