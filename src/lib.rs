use std::{collections::HashMap, marker::PhantomData};

#[derive(Clone,Debug)]
struct Rational<P: PrimeGenerator> {
    negative: bool,
    num: HashMap<i64, usize>,
    denom: HashMap<i64, usize>,
    phantom: PhantomData<P>
}

#[derive(Clone,Debug)]
/// A type representing a rational number or float.
///
/// Floats are used when the rational number would require a numerator or denominator too large to be represented using integers, or to represent `infinity` and `NaN`.
/// When possible, a [`Rational`] should be used rather than a 
/// [`Float`], as any operations using [`Float`]s will only 
/// produce [`Float`]s, losing the benefits of rational numbers.
///
/// [`Rational`]: Number::Rational
/// [`Float`]: Number::Float
pub enum Number<P: PrimeGenerator=Primes>{

    /// A rational number, represented using the factored numerator and denominator. This variant
    /// should be preferred over [`Float`].
    ///
    /// [`Float`]: Number::Float
#[allow(private_interfaces)]
    Rational(Rational<P>),

    /// A float, used when the numerator or denominator of a [`Rational`] would be
    /// too large, and to represent infinity and NaN. Any operations using [`Float`]s will produce [`Float`]s.
    ///
    /// [`Rational`]: Number::Rational
    /// [`Float`]: Number::Float
    Float(f64)
}
impl<Primes: PrimeGenerator> Number<Primes> {
  /// Creates a new, fully-simplified [`Rational`] from a numerator and denominator.
  ///
  /// This always produces a [`Rational`], not a [`Float`].
  ///
  /// [`Rational`]: Number::Rational
  /// [`Float`]: Number::Float
  ///
  /// # Examples
  ///
  /// ```
  /// use cogent::{Number, Primes};
  /// let a = <Number>::new_ratio(14, 2);
  /// assert_eq!(a, Number::from(7));
  /// ```
    pub fn new_ratio(num: i64, denom: i64) -> Self {
        let mut out = Self::Rational (Rational {
            negative: num.is_negative() ^ denom.is_negative(), 
            num: Self::primes_from_int(num),
            denom: Self::primes_from_int(denom),
            phantom: PhantomData
        });
        out.simplify();
        out
    }
  fn as_int(num: &HashMap<i64, usize>) -> i64 {
    let mut out: i64 = 1;
    if num.contains_key(&0) { return 0; }
    for (prime, power) in num {
      for _ in 0..*power {
        out *= *prime;
      }
    }
    out
  }
  fn checked_as_int(num: &HashMap<i64, usize>) -> Option<i64> {
      let mut out: i64 = 1;
      if num.contains_key(&0) { return Some(0); }
      for (prime, power) in num {
          for _ in 0..*power {
              if let Some(prod) = out.checked_mul(*prime) {
                  out = prod;
              } else {
                  return None;
              }
          }
      }
      Some(out)
  }
  fn primes_from_int(mut num: i64) -> HashMap<i64, usize> {
      if num < 0 {
          num *= -1;
      }
    let mut primes = HashMap::new();
    if num == 0 {
        primes.insert(0, 1);
        return primes;
    }
    for prime in Primes::new() {
        if prime > num { break; }
        if num % prime == 0 {
            loop {
                num /= prime;
                if let Some(power) = primes.get(&prime) {
                    primes.insert(prime, power+1);
                } else {
                    primes.insert(prime, 1);
                }
                if num % prime != 0 { break; }
            }
            if Primes::is_prime(num) { 
                primes.insert(num, 1);
                break; 
            }
        }
    }
    primes
  }
  /// Returns a raw float representation of the Number. If it's a [`Float`], this simply unwraps it, 
  /// otherwise it calculates the numerator and denominator and divides them.
  ///
  /// [`Float`]: Number::Float
  ///
  /// # Examples
  ///
  /// ```
  /// use cogent::Number;
  /// 
  /// // With a Rational:
  /// let a = <Number>::new_ratio(2, 3);
  /// assert_eq!(a.to_float(), 2.0/3.0);
  ///
  /// // With a Float:
  /// let a = <Number>::Float(3.45678);
  /// assert_eq!(a.to_float(), 3.45678);
  /// ```
  pub fn to_float(&self) -> f64 {
      match self {
          Self::Float(f) => *f,
          Self::Rational(Rational{num, denom, ..}) => {
              let num = Self::as_int(&num)*self.sign();
              let denom = Self::as_int(&denom);
              (num as f64)/(denom as f64)
          }
      }
  }
  /// Produces a new `Number` that represents the inverse of a number. 
  /// 
  /// The inverse of `0` is `infinity`. In all other cases, the inverse
  /// of a [`Rational`] is also a [`Rational`].
  ///
  /// # Examples:
  ///
  /// ```
  /// use cogent::Number;
  ///
  /// // With a Rational:
  /// let a = <Number>::new_ratio(7, 2);
  /// assert_eq!(a.invert(), Number::new_ratio(2, 7));
  ///
  /// // With a Float:
  /// let a = <Number>::Float(0.33);
  /// assert_eq!(a.invert(), Number::Float(1.0/0.33));
  /// ```
  ///
  /// [`Rational`]: Number::Rational
  /// [`Float`]: Number::Float
  pub fn invert(&self) -> Self {
      match self {
          Self::Float(f) => Self::Float(1.0/f),
          Self::Rational(rad) => if rad.num.contains_key(&0) {
              return Self::Float(f64::INFINITY*(self.sign() as f64));
            } else {
              Self::Rational(Rational{num: rad.denom.clone(), denom: rad.num.clone(), negative: rad.negative, phantom: PhantomData})
          }
      }
  }
  fn negate(&self) -> Self {
      match self {
          Self::Float(f) => Self::Float(-f),
          Self::Rational(rad) => Self::Rational(Rational{num: rad.num.clone(), denom: rad.denom.clone(), negative: !rad.negative, phantom: PhantomData})
      }
  }
  /// Returns an integer representing a number's sign: `-1` if it is negative, `1` if it is positive, zero, or `NaN`.
  ///
  /// # Examples
  ///
  /// ```
  /// use cogent::Number;
  /// let a = <Number>::from(10);
  /// assert_eq!(a.sign(), 1);
  /// assert_eq!(-(a.sign()), -1);
  ///
  /// let zero = <Number>::from(0);
  /// assert_eq!(zero.sign(), 1);
  ///
  /// let NaN = <Number>::Float(0.0/0.0);
  /// assert_eq!(NaN.sign(), 1);
  /// ```
  pub fn sign(&self) -> i64 {
      let negative = match self {
          Self::Float(f) => f.is_sign_negative(),
          Self::Rational(Rational{negative,..}) => *negative
      };
      if negative {
          -1
      } else {
          1
      }
  }
  fn simplify(&mut self) {
      match self {
          Self::Rational(Rational{ negative, num, denom, .. }) => {
              let mut new_num = HashMap::new();
              if num.contains_key(&0) {
                  new_num.insert(0, 1);
                  *self = Self::Rational(Rational{num: new_num, denom: HashMap::new(), negative: false, phantom: PhantomData});
                  return;
              }
              let mut new_denom = denom.clone();
              for (prime, power) in num {
                  if let Some(divisor) = denom.get(prime) {
                      new_denom.remove_entry(prime);
                      if divisor > power {
                          new_denom.insert(*prime, *divisor-*power);
                      } else if *power > *divisor {
                          new_num.insert(*prime, *power-*divisor);
                      }
                  } else {
                      new_num.insert(*prime, *power);
                  }
              }
              if Self::checked_as_int(&new_num).is_none() || Self::checked_as_int(&new_denom).is_none() {
                  let mut out = 1.0;
                  for (prime, power) in new_num {
                      for _ in 0..power {
                          out *= prime as f64;
                      }
                  }
                  for (prime, power) in new_denom {
                      for _ in 0..power {
                          out /= prime as f64;
                      }
                  }
                  *self = Self::Float(out)
              } else {
                  *self = Self::Rational(Rational{num: new_num, denom: new_denom, negative: *negative, phantom: PhantomData});
              }
          },
          Self::Float(_) => return,
      }
  }
  fn common_denom(&self, other: &Self) -> Option<(i64, i64, HashMap<i64, usize>)> {
      match self {
          Number::Float(_) => None,
          Number::Rational(rad) => match other {
              Number::Float(_) => None,
              Number::Rational(other) => {
                  let mut self_num_full = Self::as_int(&rad.num);
                  let mut denom = rad.denom.clone();
                  let mut other_num_full = Self::as_int(&other.num);
                  for (prime, power) in &rad.denom {
                      if let Some(o_power) = other.denom.get(prime) {
                          if o_power > power {
                              denom.insert(*prime, *o_power);
                              for _ in 0..(o_power-power) {
                                  if let Some(prod) = self_num_full.checked_mul(*prime) {
                                      self_num_full = prod;
                                  } else { 
                                      return None;
                                  }
                              }
                          } else if power > o_power {
                              for _ in 0..(power-o_power) {
                                  if let Some(prod) = other_num_full.checked_mul(*prime) {
                                      other_num_full = prod;
                                  } else { 
                                      return None;
                                  }
                              }
                          }
                      } else {
                              for _ in 0..*power {
                                  if let Some(prod) = other_num_full.checked_mul(*prime) {
                                      other_num_full = prod;
                                  } else { 
                                      return None;
                                  }
                              }
                      }
                  }
                  for (prime, power) in &other.denom {
                      if !rad.denom.contains_key(prime) {
                          denom.insert(*prime, *power);
                          for _ in 0..*power {
                              if let Some(prod) = self_num_full.checked_mul(*prime) {
                                  self_num_full = prod;
                              } else { 
                                  return None;
                              }
                          }
                      }
                  }
                  Some((self_num_full, other_num_full, denom))
              }
          }
      }
  }

  fn from_str_float(s: &str) -> Result<Self, String> {
      if let Some((int_portion, frac_portion)) = s.split_once(".") {
          let int: i64 = if int_portion.len() == 0 {
              0
          } else {
              int_portion.parse().map_err(|err|format!("{err}"))?
          };
          let frac_portion = frac_portion.trim_end_matches("0");
          let frac: i64 = if frac_portion.len() == 0 {
              0
          } else {
              frac_portion.parse().map_err(|err|format!("{err}"))?
          };
          let power = 10_i64.checked_pow(frac_portion.len() as u32).ok_or(String::from("Fractional portion is too large to be represented as an i64"))?;
          let num = int.checked_mul(power).map(|prod| prod.checked_add(frac)).flatten().ok_or(String::from("Numerator of ratio is too large to be represented as an i64"))?;
          let denom = power;
          Ok(Number::new_ratio(num, denom))
      } else {
          Err("String cannot be parsed as an integer or float".to_owned())
      }
  }
}

impl<P: PrimeGenerator> std::fmt::Display for Number<P> {
  fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(),std::fmt::Error> {
      match self {
          Self::Float(fl) => if fl.is_infinite() {
              write!(f, "{}infinity", if fl.is_sign_negative() { "-" } else { "" })
          } else {
              write!(f, "{fl}")
          },
          Self::Rational(Rational{num, denom, ..}) => {
              let denom = Self::as_int(denom);
              if denom == 1 {
                  write!(f, "{}", Self::as_int(num)*self.sign())
              } else {
                  write!(f, "({num}/{denom})",
                      num=Self::as_int(num)*self.sign(),
                  )
              }
          }
      }
  }
}

impl<P: PrimeGenerator> From<i64> for Number<P> {
  fn from(num: i64) -> Self {
    Self::new_ratio(num, 1)
  }
}
impl<P: PrimeGenerator> std::str::FromStr for Number<P> {
  type Err = String;
  fn from_str(s: &str) -> Result<Number<P>, Self::Err> {
      if s == "inf" { return Err(String::from("Could not parse into Number")); }
      if s == "infinity" { return Ok(Number::Float(f64::INFINITY)); }
      if s == "-infinity" { return Ok(Number::Float(-f64::INFINITY)); }
    if let Ok(int) = s.parse::<i64>() {
      Ok(Number::from(int))
    } else if let Ok(num) = Self::from_str_float(s){
        Ok(num)
    } else if let Ok(float) = s.parse::<f64>() {
        Ok(Number::Float(float))
    } else {
        Err(String::from("Could not parse into Number"))
    }
  }
}

impl<P: PrimeGenerator> core::ops::Mul for &Number<P> {
  type Output = Number<P>;
  fn mul(self, other: Self) -> Self::Output {
      match self {
          Number::Float(self_f) => Number::Float(self_f*other.to_float()),
          Number::Rational(rad) => match other{
              Number::Float(other_f) => Number::Float(other_f*self.to_float()),
              Number::Rational(other) => {
                  let mut num = rad.num.clone();
                  let mut denom = rad.denom.clone();
                  for (prime, power) in &other.num {
                      num.insert(*prime, power+num.get(&prime).unwrap_or(&(0 as usize)));
                  }
                  for (prime, power) in &other.denom {
                      denom.insert(*prime, power+denom.get(&prime).unwrap_or(&(0 as usize)));
                  }
                  let mut out = Number::Rational(Rational{negative: rad.negative ^ other.negative, num, denom, phantom: PhantomData});
                  out.simplify();
                  out
              }
          }
      }
  }
}

impl<P: PrimeGenerator> core::ops::Div for &Number<P> {
  type Output = Number<P>;
  fn div(self, other: Self) -> Self::Output {
    self*&other.invert()
  }
}

impl<P: PrimeGenerator> core::ops::Add for &Number<P> {
  type Output = Number<P>;
  fn add(self, other: Self) -> Self::Output {
      if let Some((self_num, other_num, denom)) = Number::common_denom(self, other) {
          let num = self_num*self.sign()+other_num*other.sign();
          let mut out = Number::Rational(Rational{num: Number::<P>::primes_from_int(num), denom, negative: num.is_negative(), phantom: PhantomData});
          out.simplify();
          out
      } else {
          Number::Float(self.to_float()+other.to_float())
      }
  }
}
impl<P: PrimeGenerator> core::ops::Sub for &Number<P> {
  type Output = Number<P>;
  fn sub(self, other: Self) -> Self::Output {
    self+&other.negate()
  }
}
impl<P: PrimeGenerator> core::ops::Neg for &Number<P> {
  type Output = Number<P>;
  fn neg(self) -> Self::Output {
    self.negate()
  }
}

impl<P: PrimeGenerator> core::ops::Mul for Number<P> {
  type Output = Self;
  fn mul(self, other: Self) -> Self {
    &self*&other
  }
}
impl<P: PrimeGenerator> core::ops::Div for Number<P> {
  type Output = Self;
  fn div(self, other: Self) -> Self {
    &self/&other
  }
}
impl<P: PrimeGenerator> core::ops::Add for Number<P> {
  type Output = Self;
  fn add(self, other: Self) -> Self {
    &self+&other
  }
}
impl<P: PrimeGenerator> core::ops::Sub for Number<P> {
  type Output = Self;
  fn sub(self, other: Self) -> Self {
    &self-&other
  }
}
impl<P: PrimeGenerator> core::ops::Neg for Number<P> {
  type Output = Self;
  fn neg(self) -> Self {
    self.negate()
  }
}
impl<P: PrimeGenerator> PartialEq for Number<P> {
  fn eq(&self, other: &Self) -> bool {
      match self {
          Self::Float(f) => *f == other.to_float(),
          Self::Rational(rad) => match other {
              Self::Float(f) => *f == self.to_float(),
              Self::Rational(other) => 
                  Self::as_int(&rad.num) == Self::as_int(&other.num)
                      && Self::as_int(&rad.denom) == Self::as_int(&other.denom)
                      && rad.negative == other.negative
          }
      }
  }
}

impl<P: PrimeGenerator> PartialOrd for Number<P> {
  fn partial_cmp(&self, other: &Self) -> Option<core::cmp::Ordering> {
      if let Some((self_num, other_num, _)) = Self::common_denom(self, other) {
          (self_num*self.sign()).partial_cmp(&(other_num*other.sign()))
      } else {
          (self.to_float()).partial_cmp(&other.to_float())
      }
  }
}

impl<P: PrimeGenerator> TryInto<i64> for Number<P> {
  type Error = ();
  fn try_into(self) -> Result<i64, Self::Error> {
      match &self {
          Self::Float(_) => Err(()),
          Self::Rational(Rational{ num, denom, .. }) => if Self::as_int(denom) == 1 {
              Ok(Self::as_int(num)*self.sign())
          } else {
              Err(())
          }
      }
  }
}

/// A trait for generating primes in order.
///
/// Implementors must also implement [`Iterator`] with
/// [`i64`] items.
pub trait PrimeGenerator: Iterator<Item=i64> {
    /// Creates a new generator.
    ///
    /// The returned generator should give `2` for the first prime.
    ///
    /// # Examples
    ///
    /// ```
    /// use cogent::{Primes, PrimeGenerator};
    ///
    /// let mut primes = Primes::new();
    /// assert_eq!(primes.next(), Some(2));
    /// assert_eq!(primes.next(), Some(3));
    /// ```
    fn new() -> Self;

    /// Checks whether a number is prime.
    ///
    /// # Examples
    ///
    /// ```
    /// use cogent::{Primes, PrimeGenerator};
    ///
    /// assert!(Primes::is_prime(2));
    /// assert!(!Primes::is_prime(-2));
    /// assert!(!Primes::is_prime(4));
    /// ```
    fn is_prime(num: i64) -> bool;
}

#[derive(Clone,Debug)]
/// The default [`PrimeGenerator`] for [`Number`].
///
/// The generator and primality test are both not very efficient or optimized, but they work well
/// enough for most purposes.
pub struct Primes { prev: i64}
impl PrimeGenerator for Primes {
  fn new() -> Self {
    Self{prev: 1}
  }
/// The primality test simply uses basic trial division up to the square root of the number, not 
/// checking even or threeven divisors after 2 and 3.
  fn is_prime(num: i64) -> bool {
    if num == 2 || num == 3 { return true; }
    if num == 1 || num%2 == 0 || num%3 == 0 { return false; }
    let mut factor = 5;
    while factor*factor <= num {
        if num%factor == 0 || num%(factor+2) == 0 {
            return false;
        }
        factor += 6;
    }
    true
  }
}

impl Iterator for Primes {
  type Item = i64;
  fn next(&mut self) -> Option<i64> {
      if self.prev == 1 { 
          self.prev = 2;
          return Some(self.prev);
      }
      if self.prev == 2 {
          self.prev = 3;
          return Some(self.prev);
      }
    self.prev += 2;
    while !Self::is_prime(self.prev) {
      self.prev += 2;
    }
    Some(self.prev)
  }
}
